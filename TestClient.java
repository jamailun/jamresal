import fr.jamailun.jamlogger.JamLogger;
import fr.jamailun.jamResal.*;
import fr.jamailun.jamResal.client.JamClient;
import java.util.Scanner;

public class TestClient {
	public static void main(String[] args) {
		new TestClient();
	}
	
	Scanner scanner;
	JamClient client;
	TestClient() {
		client = new JamClient();
		client.callbacks()
				.addCallback(StandardPackets.ID_PACKET_CANNOT_JOIN, this::serverFull)
				.addCallback(StandardPackets.ID_PACKET_WELCOME, this::welcome)
				.setConnectionCallback(this::setUsername)
				.addCallback(10, this::usernameOk)
				.addCallback(13, p -> JamLogger.info(p.readString()))
				.addCallback(14, this::receiveMessage)
				.setDisconnectionCallback(t -> {
					System.out.println("\n");
					JamLogger.error("Disconnected from server.");
					System.exit(0);
				});
		
		client.connect("localhost", 8090);
		scanner = new Scanner(System.in);
	}
	
	private void serverFull(Packet p) {
		JamLogger.error("Server was full : '" + p.readString() + "'.");
	}
	
	private void welcome(Packet p) {
		JamLogger.success("==== Connected to server ! ====");
		while(p.hasMore())
			JamLogger.info(p.readString());
	}
	
	private void receiveMessage(Packet packet) {
		String username = packet.readString();
		String msg = packet.readString();
		JamLogger.log("["+username+"] " + msg);
	}
	
	private String username = null;
	private void setUsername(Tunnel tunnel) {
		JamLogger.info("Please, enter an username.");
		JamLogger.question(true);
		username = readString();
		tunnel.sendPacket(STRUCTURE_SET_USERNAME.build(username));
	}
	
	private void usernameOk(Packet p) {
		JamLogger.success("Your username, '" + username + "' has been accepted.");
		startReadConsole(p.getTunnel());
	}
	
	private void startReadConsole(Tunnel tunnel) {
		new Thread(() -> {
			while(true) {
				String line = readString();
				if(line.startsWith("!"))
					client.sendPacketUDP(STRUCTURE_MESSAGE.build(line));
				tunnel.sendPacket(STRUCTURE_MESSAGE.build(line));
			}
		}, "consoleThread").start();
	}
	
	private String readString() {
		return scanner.nextLine();
	}
	
	private static final PacketStructure STRUCTURE_MESSAGE = new PacketStructure(15).addField(PacketStructure.FieldType.STRING);
	private static final PacketStructure STRUCTURE_SET_USERNAME = new PacketStructure(16).addField(PacketStructure.FieldType.STRING);
}
