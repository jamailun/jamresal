# JamResal

A solution to quickly do a client / server architecture.

## Concept

The concept of this libraire is to create a server and a client which will react to predefined packets.
Every packet has a ID which define it, data in it, and provides an access to the tunnel it come from.
With this tunel, you can send back answers.

## How to use it

Her's an very simple example. When a client connects to server, we send a string

### Server
```java
public class MyServer {
	public MyClient() {
		JamServer server = new JamServer(8000, 16);
		// Set the effect after a client connected.
		server.callbacks().setConnectionCallback(tunnel -> {
			Packet packet = new Packet(10);
			packet.writeString("Welcome to my server :)");
			tunnel.sendPacket(packet);
		});
		
		// Start the server.
		server.start();
	}
}
```
### Client
```java
public class MyClient {
	public MyClient() {
		JamClient client = new JamClient();
		// Set effect for the packet of id '10'
        client.callbacks().addCallback(10, packet -> {
        	String msg = packet.readString();
        	System.out.println("The packet contains the string [" + msg + "]");
        });
        
        // Start the client to a specific address.
        client.connect("127.0.0.1", 8000);
	}
}
```

So it's all about reactions to a specific packet.

## See snippets for a examples :

https://gitlab.com/jamailun/jamresal/-/snippets

