package fr.jamailun.jamResal;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class PropertiesSet {
	
	public static final String PROPERTIE_NAME = "__name__";
	public static final String PROPERTIE_IS_CLIENT = "__isClient__";
	
	private final Map<String, Object> props;
	
	public PropertiesSet() {
		props = new HashMap<>();
	}
	
	public boolean hasPropertie(String propertie) {
		return props.containsKey(propertie);
	}
	
	public void setPropertie(String propertie, Object value) {
		props.put(propertie, value);
	}
	
	public String getString(String propertie) {
		Object value = props.get(propertie);
		if(value instanceof String)
			return (String) value;
		return null;
	}
	
	public String getStringOr(String propertie, String orString) {
		String r = getString(propertie);
		return r == null ? orString : r;
	}
	
	public int getInt(String propertie) {
		Object value = props.get(propertie);
		if(value instanceof Integer)
			return (int) value;
		return 0;
	}
	
	public double getDouble(String propertie) {
		Object value = props.get(propertie);
		if(value instanceof Double)
			return (double) value;
		return 0;
	}
	
	public boolean getBoolean(String propertie) {
		Object value = props.get(propertie);
		if(value instanceof Boolean)
			return (boolean) value;
		return false;
	}
	
	public <T> T get(String propertie, Class<T> clazz) {
		Object value = props.get(propertie);
		if(clazz.isInstance(value))
			return (T) value;
		return null;
	}
	
	public <T> void applyIfExists(String propertie, Class<T> clazz, Consumer<T> consumer) {
		T value = get(propertie,clazz);
		if(value != null)
			consumer.accept(value);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("PropertiesSet[");
		boolean first = true;
		for(Map.Entry<String, Object> entry : props.entrySet()) {
			if(first)
				first = false;
			else
				builder.append(", ");
			builder.append("{").append(entry.getKey()).append(" => ").append(entry.getValue()).append("}");
		}
		return builder.append("]").toString();
	}
}
