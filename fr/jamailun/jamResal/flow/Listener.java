package fr.jamailun.jamResal.flow;

import java.util.function.*;

public class Listener<T> {
	
	private final Consumer<T> consumer;
	
	public Listener(Consumer<T> consumer) {
		this.consumer = consumer;
	}
	
	public void receive(T t) {
		consumer.accept(t);
	}
	
}
