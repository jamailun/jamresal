package fr.jamailun.jamResal.flow;

import java.util.*;
import java.util.concurrent.*;

public class EventEmitter<T>  {
	
	private final ExecutorService executor = ForkJoinPool.commonPool(); // daemon-based
	private final Set<Listener<T>> listeners = new HashSet<>();
	
	public synchronized void publish(T t) {
		executor.submit(() -> listeners.forEach(l -> l.receive(t)));
	}
	
	public void addListener(Listener<T> listener) {
		listeners.add(listener);
	}
	
}
