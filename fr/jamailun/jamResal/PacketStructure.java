package fr.jamailun.jamResal;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Util class to create a {@link fr.jamailun.jamResal.Packet }.<br>
 * Define the structure with all the fields, then
 */
public class PacketStructure {
	
	private final int id;
	private List<FieldType> fields = new ArrayList<>();
	
	/**
	 * Instanciate a new instance of the PacketStructure, with a specific id.
	 * @param id id of the packet.
	 */
	public PacketStructure(int id) {
		this.id = id;
	}
	
	/**
	 * Add a field at the current position.
	 * @param type type of the field.
	 * @return itself
	 */
	public PacketStructure addField(FieldType type) {
		fields.add(type);
		return this;
	}
	
	/**
	 * Build the packet with specific parameters.
	 * @param params parameters of the fields of the Packet.
	 * @return the instance of the Packet.
	 */
	public <T> Packet build(Object... params) {
		Packet packet = new Packet(id);
		int i = 0;
		for(FieldType type : fields) {
			type.consumer.accept(packet, params[i++]);
		}
		return packet;
	}
	
	public enum FieldType {
		BYTES_ARRAY((p,d) -> p.writeAtEnd((byte[])d)),
		BYTE((p,d) -> p.writeByte((byte)d)),
		CHAR((p,d) -> p.writeChar((char)d)),
		INT((p,d) -> p.writeInt((int)d)),
		FLOAT((p,d) -> p.writeFloat((float)d)),
		LONG((p,d) -> p.writeLong((long)d)),
		DOUBLE((p,d) -> p.writeDouble((double)d)),
		STRING((p,d) -> p.writeString((String)d));
		
		final BiConsumer<Packet, Object> consumer;
		FieldType(BiConsumer<Packet, Object> consumer) {
			this.consumer = consumer;
		}
	}
	
}
