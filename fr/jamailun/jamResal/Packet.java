package fr.jamailun.jamResal;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * A tool class to serialize data.<br>
 * The data can be obtained with {@link Packet#getBytes #getBytes()}, or the class can be serialized.
 */
public class Packet implements Serializable {

	private byte[] content;
	transient private int readPos = 0;
	transient private boolean lengthHasBeenWrote = false;
	
	/**
	 * Create a new packet, which contains a specific ID.
	 * @param id id of the packet.
	 */
	public Packet(int id) {
		content = ByteBuffer.allocate(4).putInt(id).array();
	}
	
	/**
	 * Create a new packet, with a specific data contained inside.
	 * @param bytes bytes to create the packet's with.
	 */
	public Packet(byte[] bytes) {
		content = new byte[0];
		writeAtBegin(bytes);
	}
	
	public Packet(Packet packet) {
		content = packet.getBytes();
	}
	
	/**
	 * Create an empty packet.
	 */
	public Packet() {
		content = new byte[0];
	}
	
	/**
	 * Get all bytes contained in the array.
	 * @return the whole bytes array of the packet.
	 */
	public byte[] getBytes() {
		return content;
	}
	
	/**
	 * Write bytes in the packet.
	 * @param addBytes bytes array to add.
	 */
	public void writeAtBegin(byte[] addBytes) {
		byte[] bytes = new byte[addBytes.length + content.length];
		System.arraycopy(addBytes,0, bytes, 0, addBytes.length);
		System.arraycopy(content,0, bytes, addBytes.length, content.length);
		this.content = bytes;
	}
	
	public void writeAtEnd(byte[] addBytes) {
		byte[] bytes = new byte[addBytes.length + content.length];
		System.arraycopy(content,0, bytes, 0, content.length);
		System.arraycopy(addBytes,0, bytes, content.length, addBytes.length);
		this.content = bytes;
	}
	
	/**
	 * Write the lenght at the begin of the packet<br/>
	 * If this method has already been called previously, does nothing.
	 * @return true if the call has been effective.
	 */
	public boolean writeLength() {
		if(lengthHasBeenWrote)
			return false;
		writeAtBegin(ByteBuffer.allocate(4).putInt(content.length).array());
		return true;
	}
	
	public void unreadLastN(int amount) {
		if(readPos - amount < 0)
			throw new IllegalArgumentException("Cannot go under 0 in read pos.");
		readPos -= amount;
	}
	
	/**
	 * Get if the length has already been wrote.
	 * @return true if it's the case.
	 */
	public boolean isLengthHasBeenWrote() {
		return lengthHasBeenWrote;
	}
	
	/**
	 * Reset the packet, erease the data it contains.
	 */
	public void reset() {
		content = new byte[0];
		readPos = 0;
		lengthHasBeenWrote = false;
	}
	
	/**
	 * Write an int in the packet
	 * @param data : int to add
	 */
	public void writeInt(int data) {
		writeAtEnd(ByteBuffer.allocate(4).putInt(data).array());
	}
	
	/**
	 * Write a long in the packet
	 * @param data : long to add
	 */
	public void writeLong(long data) {
		writeAtEnd(ByteBuffer.allocate(8).putLong(data).array());
	}
	
	/**
	 * Write a byte in the packet
	 * @param data : byte to add
	 */
	public void writeByte(byte data) {
		writeAtEnd(new byte[] {data});
	}
	
	/**
	 * Write a char in the packet
	 * @param data : char to add
	 */
	public void writeChar(char data) {
		writeAtEnd(ByteBuffer.allocate(2).putChar(data).array());
	}
	
	/**
	 * Write a double in the packet
	 * @param data : double to add
	 */
	public void writeDouble(double data) {
		writeAtEnd(ByteBuffer.allocate(8).putDouble(data).array());
	}
	
	/**
	 * Write a float in the packet
	 * @param data : float to add
	 */
	public void writeFloat(float data) {
		writeAtEnd(ByteBuffer.allocate(4).putFloat(data).array());
	}
	
	/**
	 * Write a string in the packet
	 * @param data : string to add
	 */
	public void writeString(String data) {
		writeString(data, StandardCharsets.UTF_8);
	}
	
	/**
	 * Write a string in the packet
	 * @param data : string to add
	 * @param charset : charset to serialize the string
	 */
	public void writeString(String data, Charset charset) {
		if(data == null)
			return;
		byte[] converted = data.getBytes(charset);
		writeInt(converted.length);
		writeAtEnd(converted);
	}
	
	/**
	 * Get the unread amount of bytes.
	 * @return the length of the amount of unread bytes.
	 */
	public int remainingBytes() {
		return content.length - readPos;
	}
	
	/**
	 * Get the length of the packet.
	 * @return the amount of bytes in the packet.
	 */
	public int length() {
		return content.length;
	}
	
	/**
	 * Read a certain amount of unread bytes.
	 * @param length the amount of bytes to read.
	 * @return a bytes array of data.
	 * @throws IllegalArgumentException if the asked length is greater than the amount of unread bytes.
	 */
	public byte[] readBytes(int length) {
		if(remainingBytes() < length)
			throw new IllegalArgumentException("Called for ["+length+"] bytes. Only remaining ["+remainingBytes()+"].");
		byte[] bytes = new byte[length];
		System.arraycopy(content, readPos, bytes, 0, length);
		readPos += length;
		return bytes;
	}
	
	/**
	 * Read the length of the packet.
	 * <br>Should be the first method to call when a packet is received.
	 * @return the length of the packet (read it)
	 * @throws IllegalStateException if data has been read previously.
	 */
	public int readLength() {
		if(readPos != 0)
			throw new IllegalStateException("Could not read length of the packet. It already has been read.");
		return readInt();
	}
	
	/**
	 * Read an int in the packet.
	 * @return the int to be read.
	 */
	public int readInt() {
		return ByteBuffer.wrap(readBytes(4)).getInt();
	}
	
	/**
	 * Read a long in the packet.
	 * @return the long to be read.
	 */
	public long readLong() {
		return ByteBuffer.wrap(readBytes(8)).getLong();
	}
	
	/**
	 * Read a byte in the packet.
	 * @return the byte to be read.
	 */
	public byte readByte() {
		return readBytes(1)[0];
	}
	
	/**
	 * Read a char in the packet.
	 * @return the char to be read.
	 */
	public char readChar() {
		return ByteBuffer.wrap(readBytes(2)).getChar();
	}
	
	/**
	 * Read a double in the packet.
	 * @return the double to be read.
	 */
	public double readDouble() {
		return ByteBuffer.wrap(readBytes(8)).getDouble();
	}
	
	/**
	 * Read a float in the packet.
	 * @return the float to be read.
	 */
	public float readFloat() {
		return ByteBuffer.wrap(readBytes(4)).getFloat();
	}
	
	/**
	 * Read a string in the packet.
	 * @return the string to be read.
	 */
	public String readString() {
		return readString(StandardCharsets.UTF_8);
	}
	
	/**
	 * Read a string in the packet.
	 * @param charset the charset to serialie the string
	 * @return the string to be read.
	 */
	public String readString(Charset charset) {
		int stringSize = readInt();
		return new String(readBytes(stringSize), charset);
	}
	
	@Override
	public String toString() {
		return "Packet{" + Arrays.toString(content) + "}";
	}
	
	/**
	 * Get if the packet still has an unred part.
	 * @return true if there is more.
	 */
	public boolean hasMore() {
		return readPos < content.length;
	}
	
	transient private Tunnel tunnel;
	
	public Tunnel getTunnel() {
		return tunnel;
	}
	
	public void setTunnel(Tunnel tunnel) {
		this.tunnel = tunnel;
	}
}
