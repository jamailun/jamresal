package fr.jamailun.jamResal;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static fr.jamailun.jamResal.PacketStructure.FieldType.*;

public class StandardPackets {
	
	public static final int ID_PACKET_TERMINATE = 0;
	public static final int ID_PACKET_WELCOME = 1;
	public static final int ID_PACKET_SUCCESS = 2;
	public static final int ID_PACKET_CANNOT_JOIN = 3;
	public static final int ID_PACKET_ALLOW_UDP = 4;
	
	public static final PacketStructure PACKET_TERMINATE = new PacketStructure(ID_PACKET_TERMINATE);
	public static final PacketStructure PACKET_OK = new PacketStructure(ID_PACKET_SUCCESS);
	public static final PacketStructure PACKET_ACCEPT_CONNECTION = new PacketStructure(ID_PACKET_WELCOME).addField(STRING);
	public static final PacketStructure PACKET_REFUSE_CONNECTION = new PacketStructure(ID_PACKET_CANNOT_JOIN).addField(STRING);
	public static final PacketStructure PACKET_ALLOW_UDP = new PacketStructure(ID_PACKET_ALLOW_UDP).addField(INT);
	
	public static final List<Integer> FORBIDDEN_IDS = Collections.unmodifiableList(Arrays.asList(
			ID_PACKET_TERMINATE,
			ID_PACKET_WELCOME,
			ID_PACKET_SUCCESS,
			ID_PACKET_CANNOT_JOIN,
			ID_PACKET_ALLOW_UDP
	));
	
}
