package fr.jamailun.jamResal;

import fr.jamailun.jamResal.flow.Listener;

import java.io.Closeable;

public interface Tunnel extends Closeable {
	
	boolean isActive();
	
	void sendPacket(Packet packet);
	
	void addListener(Listener<Packet> listener);
	
	void startListening();
	
	PropertiesSet properties();
	
}
