package fr.jamailun.jamResal.server;

import java.net.InetAddress;

public class ClientInfo {
	
	private static int CURRENT_ID = 1;
	
	private int udpPort;
	private final int id;
	private final InetAddress endPoint;
	
	public ClientInfo(InetAddress endPoint) {
		this.endPoint = endPoint;
		id = CURRENT_ID++;
	}
	
	public int getId() {
		return id;
	}
	
	public void setPortUDP(int udpPort) {
		this.udpPort = udpPort;
	}
	
	public boolean hasPortUDP() {
		return udpPort != -1;
	}
	
	public int getPortUDP() {
		return udpPort;
	}
	
	public InetAddress getEndPoint() {
		return endPoint;
	}
	
	@Override
	public String toString() {
		return "ClientInfo{" +
				"id=" + id +
				", udpPort=" + udpPort +
				'}';
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ClientInfo)
			return ((ClientInfo)obj).id == id;
		return false;
	}
}
