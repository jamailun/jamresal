package fr.jamailun.jamResal.server;

import fr.jamailun.jamResal.*;
import fr.jamailun.jamResal.flow.*;
import fr.jamailun.jamlogger.*;

import java.util.Arrays;

public class TunnelBranchUDP implements Tunnel {
	
	private final TunnelMainUDP main;
	private final ClientInfo info;
	private final EventEmitter<Packet> emitter;
	private final PropertiesSet properties;
	
	TunnelBranchUDP(TunnelMainUDP main, ClientInfo info, PropertiesSet properties) {
		emitter = new EventEmitter<>();
		this.properties = properties;
		this.main = main;
		this.info = info;
	}
	
	@Override
	public void sendPacket(Packet packet) {
		main.sendPacket(packet, info.getEndPoint(), info.getPortUDP());
	}
	
	@Override
	public void addListener(Listener<Packet> listener) {
		emitter.addListener(listener);
	}
	
	void receive(Packet packet) {
		packet.setTunnel(this);
		emitter.publish(packet);
	}
	
	ClientInfo getClient() {
		return info;
	}
	
	@Override
	public void startListening() {
		JamLogger.error("Ce n'est pas la peine de faire écouter un TunnelBranchUDP...");
	}
	
	@Override
	public PropertiesSet properties() {
		return properties;
	}
	
	@Override
	public void close() {
		main.close();
	}
	
	@Override
	public boolean isActive() {
		return main.isActive();
	}
}
