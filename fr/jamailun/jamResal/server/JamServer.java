package fr.jamailun.jamResal.server;

import fr.jamailun.jamResal.*;
import fr.jamailun.jamlogger.*;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Predicate;

public class JamServer implements Closeable {
	
	private final ServerSocket hubSocket;
	private boolean open = false;
	private final Thread thread;
	
	private final ClientsManager clients;
	
	private final CallbackHandler callbacks;
	
	public JamServer(int port) throws IOException {
		this(port, -1);
	}
	
	public JamServer(int port, int maxClients) throws IOException {
		// Utils
		clients = new ClientsManager(maxClients, port);
		hubSocket = new ServerSocket(port);
		// Callback
		callbacks = new ServerCallbackHandler(clients);
		// Thread
		thread = new Thread(() -> {
			while(open) {
				try {
					JamLogger.log("Server listening on port ("+port+").");
					Socket newSocket = hubSocket.accept();
					JamLogger.log("Accepted socket from ("+newSocket.getInetAddress().getHostAddress() + ":"+newSocket.getPort()+").");
					
					clients.addClient(newSocket, callbacks);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}, "ultraServer-Thread");
	}
	
	public void start() {
		open = true;
		thread.start();
	}
	
	public void sendToAll(Packet packet) {
		clients.sendToAllClients(packet);
	}
	public void sendToAllPredicate(Predicate<PropertiesSet> predicate, Packet packet) {
		clients.sendToAllPredicate(predicate, packet);
	};
	
	@Override
	public void close() throws IOException {
		open = false;
		thread.interrupt();
		hubSocket.close();
	}
	
	public CallbackHandler callbacks() {
		return callbacks;
	}
	
	public int getHowManyClients() {
		return clients.getHowManyClients();
	}
	
	public int getMaximumAmountOfClients() {
		return clients.getMaxClients();
	}
}
