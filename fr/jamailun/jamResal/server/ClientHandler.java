package fr.jamailun.jamResal.server;

import fr.jamailun.jamlogger.*;
import fr.jamailun.jamResal.*;
import fr.jamailun.jamResal.flow.*;

import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.function.BiConsumer;

public class ClientHandler implements Closeable {
	
	private TunnelTCP tunnelTCP;
	private final Socket socket;
	private final ClientInfo info;
	private final CallbackHandler callbacks;
	private final PropertiesSet properties;
	private final BiConsumer<ClientInfo, PropertiesSet> udpOpener;
	
	ClientHandler(ClientsManager manager, Socket socket, ClientInfo info, CallbackHandler callbacks) {
		this.udpOpener = manager.getOpenUDPConsumer();
		this.socket = socket;
		this.info = info;
		this.callbacks = callbacks;
		properties = new PropertiesSet();
		try {
			tunnelTCP = TunnelTCP.createFromSocket(socket, callbacks, properties);
			tunnelTCP.addListener(new Listener<>(this::handlePacket));
			tunnelTCP.startListening();
			tunnelTCP.properties().setPropertie("client", info);
			tunnelTCP.properties().setPropertie("__name__", "serverTunnel_TCP_"+info.getId());
			tunnelTCP.properties().setPropertie("__manager__", manager);
			callbacks.handleConnect(tunnelTCP);
		} catch (IOException e) {
			JamLogger.error("Could not create tunnel for client in slot " + info.getId() + " : " + e.getMessage());
		}
	}
	
	private void handlePacket(Packet p) {
		int id = p.readInt();
		switch (id) {
			case StandardPackets.ID_PACKET_TERMINATE:
				try { close(); } catch(IOException e) { e.printStackTrace(); }
				break;
			case StandardPackets.ID_PACKET_ALLOW_UDP:
				int clientListenPort = p.readInt();
				openUDP(clientListenPort);
				break;
			default:
				p.unreadLastN(4);
				callbacks.handlePacket(p, tunnelTCP);
		}
	}
	
	private void openUDP(int clientPort) {
		JamLogger.info("Opening udp tunnel. Targetting ["+socket.getInetAddress() + ":"+ clientPort+"]");
		info.setPortUDP(clientPort);
		udpOpener.accept(info, properties);
	}
	
	public Tunnel getTunnelTCP() {
		return tunnelTCP;
	}
	
	public ClientInfo getInfo() {
		return info;
	}
	
	@Override
	public String toString() {
		return "ClientHandler{state="+socket.isConnected()+", info="+ info +"}";
	}
	
	@Override
	public void close() throws IOException {
		tunnelTCP.close();
		socket.close();
	}
	
	CallbackHandler getCallbacks() {
		return callbacks;
	}
}
