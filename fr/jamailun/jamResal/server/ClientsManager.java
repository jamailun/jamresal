package fr.jamailun.jamResal.server;

import fr.jamailun.jamResal.*;
import fr.jamailun.jamlogger.*;

import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.ForkJoinPool;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class ClientsManager {
	
	private final int maxClients, localPort;
	private final Collection<ClientHandler> handlers;
	private final TunnelMainUDP mainUDP;
	
	ClientsManager(int maxClients, int localPort) {
		this.maxClients = maxClients;
		this.localPort = localPort;
		handlers = new HashSet<>();
		mainUDP = new TunnelMainUDP(this, localPort);
		mainUDP.startListening();
	}
	
	int getLocalPort() {
		return localPort;
	}
	
	void addClient(Socket socket, CallbackHandler callbacks) {
		if(maxClients != -1 && handlers.size() >= maxClients) {
			JamLogger.warning("Server is full, send server full and disconnect last client..");
			try(TunnelTCP tunnel = TunnelTCP.createFromSocket(socket, callbacks)) {
				tunnel.sendPacket(StandardPackets.PACKET_REFUSE_CONNECTION.build("Server full"));
				Thread.sleep(2000);
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
			return;
		}
		
		ClientInfo info = new ClientInfo(socket.getInetAddress());
		handlers.add(new ClientHandler(this, socket, info, callbacks));
		
		JamLogger.log("New client connected : " + info + ".");
	}
	
	public Tunnel getTunnelTCP(int id) {
		ClientHandler h = getHandler(id);
		return h == null ? null : h.getTunnelTCP();
	}
	
	public Tunnel getTunnelUDP(int id) {
		return mainUDP.getBranch(id);
	}
	
	public ClientHandler getHandler(int id) {
		return handlers.stream().filter(h -> h.getInfo().getId() == id).findFirst().orElse(null);
	}
	
	void sendToAllClients(Packet packet) {
		handlers.forEach(h -> h.getTunnelTCP().sendPacket(new Packet(packet)));
	}
	
	int howManyClients() {
		int tt = 0;
		for (ClientHandler handler : handlers)
			if (handler != null)
				tt++;
		return tt;
	}
	
	public void disconnect(ClientInfo info) {
		ForkJoinPool.commonPool().execute(() -> {
			try {
				getHandler(info.getId()).close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			removeClient(info);
		});
	}
	
	void sendToAllPredicate(Predicate<PropertiesSet> predicate, Packet packet) {
		handlers.stream()
				.filter(clientHandler -> predicate.test(clientHandler.getTunnelTCP().properties()))
				.forEach(h -> h.getTunnelTCP().sendPacket(new Packet(packet)));
	}
	
	void removeClient(ClientInfo client) {
		handlers.removeIf(c -> c.getInfo().getId() == client.getId());
		JamLogger.log("Removed client " + client);
	}
	
	public BiConsumer<ClientInfo, PropertiesSet> getOpenUDPConsumer() {
		return (ci,props) -> mainUDP.addClient(ci, getHandler(ci.getId()).getCallbacks(), props);
	}
	
	public int getHowManyClients() {
		return handlers.size();
	}
	
	public int getMaxClients() {
		return maxClients;
	}
}
