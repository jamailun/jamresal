package fr.jamailun.jamResal.server;

import fr.jamailun.jamResal.*;

public class ServerCallbackHandler extends CallbackHandler {
	
	private final ClientsManager clients;
	
	ServerCallbackHandler(ClientsManager clients) {
		super();
		this.clients = clients;
	}
	
	public void handleDisconnect(PropertiesSet context) {
		super.handleDisconnect(context);
		ClientInfo client = context.get("client", ClientInfo.class);
		clients.disconnect(client);
	}
	
}
