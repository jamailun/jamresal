package fr.jamailun.jamResal.server;

import fr.jamailun.jamResal.*;
import fr.jamailun.jamResal.flow.Listener;
import fr.jamailun.jamlogger.*;

import java.io.Closeable;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;

public class TunnelMainUDP implements Closeable {

	private DatagramSocket socketUdp;
	private Thread thread;
	private boolean open;
	private final Executor executor = ForkJoinPool.commonPool();
	
	private final Set<TunnelBranchUDP> branches;
	
	TunnelMainUDP(ClientsManager manager, int localPort) {
		branches = new HashSet<>();
		try {
			socketUdp = new DatagramSocket(localPort);
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	void addClient(ClientInfo client, CallbackHandler callbacks, PropertiesSet properties) {
		TunnelBranchUDP branch = new TunnelBranchUDP(this, client, properties);
		branch.addListener(new Listener<>(p -> callbacks.handlePacket(p, branch)));
		branches.add(branch);
	}
	
	void disconnectClient(ClientInfo client) {
		TunnelBranchUDP branch = getBranch(client.getId());
		if(branch == null)
			return;
		branch.close();
		branches.removeIf(c -> c.getClient().getId() == client.getId());
	}
	
	TunnelBranchUDP getBranch(int id) {
		return branches.stream().filter(c -> c.getClient().getId() == id).findFirst().orElse(null);
	}
	
	TunnelBranchUDP getBranch(InetAddress address, int port) {
		return branches.stream().filter(c -> c.getClient().getEndPoint().equals(address) && c.getClient().getPortUDP() == port).findFirst().orElse(null);
	}
	
	public void startListening() {
		open = true;
		thread = new Thread(() -> {
			byte[] intBuff = new byte[4];
			while(open) {
				byte[] buffer = new byte[2048];
				try {
					DatagramPacket dtp = new DatagramPacket(buffer, buffer.length);
					socketUdp.receive(dtp);
					System.arraycopy(buffer,0, intBuff, 0, 4);
					int packetSize = ByteBuffer.wrap(intBuff).getInt();
					byte[] bytes = new byte[packetSize];
					System.arraycopy(buffer,4, bytes, 0, packetSize);
					// Packet obtenu
					Packet packet = new Packet(bytes);
					getBranch(dtp.getAddress(), dtp.getPort()).receive(packet);
				} catch (IOException e) {
					if(open)
						e.printStackTrace();
				}
			}
		});
		thread.start();
	}
	
	void sendPacket(Packet packet, InetAddress clientAddress, int clientPort) {
		executor.execute(() -> {
			packet.writeLength();
			byte[] bytes = packet.getBytes();
			DatagramPacket dtg = new DatagramPacket(bytes, bytes.length, clientAddress, clientPort);
			try {
				socketUdp.send(dtg);
			} catch(SocketException se) {
				if(isActive())
					se.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
	
	@Override
	public void close() {
		open = false;
		if(socketUdp != null)
			socketUdp.close();
		if(thread != null)
			thread.interrupt();
	}
	public boolean isActive() {
		return open;
	}
}
