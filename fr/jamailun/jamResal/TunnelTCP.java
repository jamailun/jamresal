package fr.jamailun.jamResal;

import fr.jamailun.jamlogger.*;
import fr.jamailun.jamResal.flow.*;
import fr.jamailun.jamResal.server.*;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

public class TunnelTCP implements Tunnel {
	
	public static TunnelTCP createFromSocket(Socket socket, CallbackHandler callbacks, PropertiesSet properties) throws IOException {
		return new TunnelTCP(socket.getInputStream(), socket.getOutputStream(), callbacks, properties);
	}
	
	public static TunnelTCP createFromSocket(Socket socket, CallbackHandler callbacks) throws IOException {
		return new TunnelTCP(socket.getInputStream(), socket.getOutputStream(), callbacks, null);
	}
	
	private final ExecutorService executor = ForkJoinPool.commonPool();
	private boolean open = true;
	private final InputStream input;
	private final OutputStream output;
	private final EventEmitter<Packet> emitter;
	private Thread thread;
	private final CallbackHandler callbacks;
	private final PropertiesSet properties;
	
	public TunnelTCP(InputStream inputStream, OutputStream outputStream, CallbackHandler callbacks, PropertiesSet properties) {
		input = new DataInputStream(inputStream);
		output = new DataOutputStream(outputStream);
		emitter = new EventEmitter<>();
		this.callbacks = callbacks;
		this.properties = properties == null ? new PropertiesSet() : properties;
	}
	
	@Override
	public void startListening() {
		thread = new Thread(() -> {
			while(open) {
				byte[] intBuffer = new byte[4];
				int amoutReaded = 0;
				try {
					amoutReaded = input.read(intBuffer);
					if(amoutReaded != 4) {
						JamLogger.error("TunnelTCP unexpectedly received only " + amoutReaded + " bytes.");
						continue;
					}
					int bytesToRead = ByteBuffer.wrap(intBuffer).getInt();
					byte[] bytes = new byte[bytesToRead];
					amoutReaded = input.read(bytes, 0, bytesToRead);
					if(amoutReaded < bytesToRead)
						JamLogger.warning("Didn't read everything...");
					Packet packet = new Packet(bytes);
					emitter.publish(packet);
				} catch (BufferUnderflowException bue) {
					if(amoutReaded == 0)
						try {
							close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					else
						JamLogger.error("Underflow exeption on reading tunnel. Got " + Arrays.toString(intBuffer) + " bytes.");
				} catch(SocketException se) {
					try {
						close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (IOException e) {
					if(open)
						e.printStackTrace();
				}
			}
		}, "Thread-Tunnel-TCP");
		thread.start();
	}
	
	
	@Override
	public PropertiesSet properties() {
		return properties;
	}
	
	@Override
	public void sendPacket(Packet packet) {
		executor.submit(() -> {
			packet.writeLength();
			try {
				output.write(packet.getBytes());
			} catch (SocketException e) {
				// pas la peine, on le fait déjà dans le close. callbacks.handleDisconnect(properties());
				try {
					this.close();
				} catch (IOException e1) {
					e1.printStackTrace();
					callbacks.handleDisconnect(properties());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
	
	@Override
	public void addListener(Listener<Packet> listener) {
		emitter.addListener(listener);
	}
	
	@Override
	public synchronized void close() throws IOException {
		if(!open)
			return;
		
		callbacks.handleDisconnect(properties);
		
		if(thread != null)
			thread.interrupt();
		open = false;
		input.close();
		output.close();
		JamLogger.warning("Tunnel terminated ("+Thread.currentThread().getName()+properties.getStringOr("__name__", "")+")");
	}
	
	@Override
	public boolean isActive() {
		return open;
	}
}
