package fr.jamailun.jamResal;

import fr.jamailun.jamlogger.*;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class CallbackHandler {
	
	private final Executor callbackExecutor = ForkJoinPool.commonPool();
	private final Map<Integer, Collection<Consumer<Packet>>> callbacks;
	private Consumer<Packet> defaultCallback;
	private Consumer<PropertiesSet> disconnectCallback;
	private Consumer<Tunnel> connectionCallback;
	private Consumer<String> forbiddenCallback;
	
	public CallbackHandler() {
		callbacks = new HashMap<>();
		defaultCallback = generateDefaultCallback();
		disconnectCallback = x -> {
			System.out.println("[Default handler] Disconnected with packet > "+x);
		};
		connectionCallback = x -> {
			System.out.println("[Default handler] Connected with packet > "+x);
		};
		forbiddenCallback = x -> {
			System.out.println("[Default handler] Could not connect to server > "+x);
		};
	}
	
	/**
	 * Add a callback for a specific packet id.
	 * @param packetID unique ID of this callback.
	 * @param callback handler of the correspondinf packet.
	 * @throws IllegalArgumentException if bad packetID
	 */
	public CallbackHandler addCallback(int packetID, Consumer<Packet> callback) {
		if(StandardPackets.FORBIDDEN_IDS.contains(packetID))
			throw new IllegalArgumentException("Cannot add an handler with ID '" + packetID + "' : it's already used in standard packets.");
		if(!callbacks.containsKey(packetID))
			callbacks.put(packetID, new HashSet<>());
		callbacks.get(packetID).add(callback);
		return this;
	}
	
	/**
	 * Set the default callback, used for unrecognized packets id.
	 * @param callback the consumer to handle the packet.
	 */
	public CallbackHandler setDefaultCallback(Consumer<Packet> callback) {
		this.defaultCallback = callback;
		return this;
	}
	
	/**
	 * Set the disconnection callback, called is disconnected from the other end.
	 * @param callback the consumer to handle the packet.
	 */
	public CallbackHandler setDisconnectionCallback(Consumer<PropertiesSet> callback) {
		this.disconnectCallback = callback;
		return this;
	}
	
	/**
	 * Set the connection callback, called when is connected to the other end.
	 * @param callback the consumer to handle the packet.
	 */
	public CallbackHandler setConnectionCallback(Consumer<Tunnel> callback) {
		this.connectionCallback = callback;
		return this;
	}
	
	/**
	 * Set the forbidden callback, called when the connection is forbidden.
	 * @param callback the consumer to handle the packet.
	 */
	public CallbackHandler setForbiddenCallback(Consumer<String> callback) {
		this.forbiddenCallback = callback;
		return this;
	}
	
	public void handlePacket(Packet packet, Tunnel source) {
		callbackExecutor.execute(() -> {
			int id = packet.readInt();
			packet.setTunnel(source);
			callbacks.getOrDefault(id, Collections.singletonList(defaultCallback)).forEach(c -> c.accept(packet));
		});
	}
	
	public void handleConnect(Tunnel tunnel) {
		callbackExecutor.execute(() -> connectionCallback.accept(tunnel));
	}
	
	public void handleForbidden(String message) {
		callbackExecutor.execute(() -> forbiddenCallback.accept(message));
	}
	
	public void handleDisconnect(PropertiesSet context) {
		callbackExecutor.execute(() -> disconnectCallback.accept(context));
	}
	
	private static Consumer<Packet> generateDefaultCallback() {
		return p -> {
			p.unreadLastN(4);
			JamLogger.warning("Packet not handled. Id = " + p.readInt()+". Remaing bytes = " + p.remainingBytes() + ".");
		};
	}
}
