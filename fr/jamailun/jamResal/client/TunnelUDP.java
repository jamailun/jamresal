package fr.jamailun.jamResal.client;

import fr.jamailun.jamResal.*;
import fr.jamailun.jamResal.flow.*;
import fr.jamailun.jamlogger.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;

public class TunnelUDP implements Tunnel {
	
	private final Executor executor = ForkJoinPool.commonPool();
	private Thread thread;
	private boolean open;
	
	private final PropertiesSet properties;
	private DatagramSocket socket;
	private final InetAddress targetAddress;
	private final int targetPort;
	
	private final EventEmitter<Packet> emitter;
	
	public TunnelUDP(int port, InetAddress targetAddress, int targetPort) {
		this.targetAddress = targetAddress;
		this.targetPort = targetPort;
		emitter = new EventEmitter<>();
		properties = new PropertiesSet();
		try {
			socket = new DatagramSocket(port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	public int getLocalListeningPort() {
		return socket.getLocalPort();
	}
	
	@Override
	public void sendPacket(Packet packet) {
		executor.execute(() -> {
			packet.writeLength();
			byte[] bytes = packet.getBytes();
			DatagramPacket dt = new DatagramPacket(bytes, bytes.length, targetAddress, targetPort);
			try {
				packet.writeLength();
				socket.send(dt);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
	
	@Override
	public void addListener(Listener<Packet> listener) {
		emitter.addListener(listener);
	}
	
	@Override
	public void startListening() {
		open = true;
		thread = new Thread(() -> {
			byte[] intBuff = new byte[4];
			while(open) {
				byte[] buffer = new byte[1024];
				try {
					DatagramPacket dtp = new DatagramPacket(buffer, buffer.length);
					socket.receive(dtp);
					System.arraycopy(buffer,0, intBuff, 0, 4);
					int packetSize = ByteBuffer.wrap(intBuff).getInt();
					byte[] bytes = new byte[packetSize];
					System.arraycopy(buffer,4, bytes, 0, packetSize);
					Packet packet = new Packet(bytes);
					emitter.publish(packet);
				} catch (IOException e) {
					if(open)
						e.printStackTrace();
				}
			}
		});
		thread.start();
	}
	
	@Override
	public PropertiesSet properties() {
		return properties;
	}
	
	@Override
	public void close() {
		open = false;
		if(socket != null)
			socket.close();
		if(thread != null)
			thread.interrupt();
	}
	
	@Override
	public boolean isActive() {
		return open;
	}
}
