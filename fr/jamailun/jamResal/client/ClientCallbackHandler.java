package fr.jamailun.jamResal.client;

import fr.jamailun.jamResal.*;

public class ClientCallbackHandler extends CallbackHandler {
	
	private final JamClient client;
	
	ClientCallbackHandler(JamClient client) {
		super();
		this.client = client;
	}
	
	public void handleDisconnect(PropertiesSet context) {
		super.handleDisconnect(context);
		client.sendPacketTCP(StandardPackets.PACKET_TERMINATE.build("end."));
	}
	
}
