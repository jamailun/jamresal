package fr.jamailun.jamResal.client;

import fr.jamailun.jamlogger.*;
import fr.jamailun.jamResal.*;
import fr.jamailun.jamResal.flow.*;

import java.io.Closeable;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ForkJoinPool;

/**
 * Create a new client, who is able to communicate with the server.
 */
public class JamClient implements Closeable {
	
	private Thread thread;
	private Socket socket;
	private Tunnel tunnelTCP;
	private TunnelUDP tunnelUDP;
	
	private final CallbackHandler callbacks;
	
	private int amountOfConnectionTries = 5;
	private int connectTries = 0;
	
	private boolean willStartUDP = true;
	
	public JamClient() {
		callbacks = new ClientCallbackHandler(this);
	}
	
	private String serverAddress;
	private int serverPort;
	
	public void connect(String serverAddress, int serverPort) {
		if(tunnelTCP != null)
			throw new RuntimeException("This client is already connected !");
		this.serverAddress = serverAddress;
		this.serverPort = serverPort;
		
		thread = new Thread(() -> {
			try {
				socket = new Socket(serverAddress, serverPort);
				connectTries = 0;
				tunnelTCP = TunnelTCP.createFromSocket(socket, callbacks);
				tunnelTCP.addListener(new Listener<>(this::handlePacket));
				tunnelTCP.properties().setPropertie("__name__", "ultraClientTunnel");
				tunnelTCP.properties().setPropertie("__isClient__", true);
				tunnelTCP.startListening();
				callbacks.handleConnect(tunnelTCP);
				if(willStartUDP)
					startUDP();
			} catch(ConnectException ce) {
				JamLogger.warning("Connection timed out.");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(++connectTries >= amountOfConnectionTries) {
					JamLogger.error("Could not connect to server. Terminate client.");
					return;
				}
				connect(serverAddress, serverPort);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		thread.start();
	}
	
	public void sendPacketTCP(Packet packet) {
		if( ! tunnelTCP.isActive() )
			throw new IllegalStateException("Tunnel TCP is not open.");
		tunnelTCP.sendPacket(packet);
	}
	
	public void sendPacketUDP(Packet packet) {
		if( ! tunnelUDP.isActive() )
			throw new IllegalStateException("Tunnel UDP is not open.");
		tunnelUDP.sendPacket(packet);
	}
	
	private void startUDP() {
		try {
			tunnelUDP = new TunnelUDP(0, InetAddress.getByName(serverAddress), serverPort);
			tunnelUDP.startListening();
			tunnelTCP.sendPacket(StandardPackets.PACKET_ALLOW_UDP.build(tunnelUDP.getLocalListeningPort()));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	private void handlePacket(Packet packet) {
		int id = packet.readInt();
		switch (id) {
			case StandardPackets.ID_PACKET_TERMINATE:
				close();
				break;
			case StandardPackets.ID_PACKET_WELCOME:
				callbacks.handleConnect(tunnelTCP);
				break;
			case StandardPackets.ID_PACKET_CANNOT_JOIN:
				callbacks.handleForbidden(packet.readString());
				break;
			default:
				packet.unreadLastN(4);
				callbacks.handlePacket(packet, tunnelTCP);
		}
	}
	
	public CallbackHandler callbacks() {
		return callbacks;
	}
	
	@Override
	public void close() {
		if(tunnelTCP == null)
			return;
		tunnelTCP.sendPacket(StandardPackets.PACKET_TERMINATE.build("closed"));

		ForkJoinPool.commonPool().execute(() -> {
			if(thread != null)
				thread.interrupt();
			if(socket != null && socket.isConnected()) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			callbacks.handleDisconnect(tunnelTCP.properties());
		});
	}
	
	public JamClient setNoUDP() {
		willStartUDP = false;
		return this;
	}
	
	public JamClient setAmountOfConnectionTries(int amount) {
		this.amountOfConnectionTries = amount;
		return this;
	}
}
