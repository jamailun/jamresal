package fr.jamailun.jamlogger;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JamLogger {
	
	static PrintStream stream = System.out;
	static SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
	
	public static void log(String message) {
		stream.println(
			ConsoleColor.WHITE + "[>]" + prefix
			+ ConsoleColor.CYAN + "[" + ConsoleColor.WHITE_BRIGHT + formatter.format(new Date()) + ConsoleColor.CYAN + "]"
			+ ConsoleColor.CYAN + " > " + ConsoleColor.RESET + message+ ConsoleColor.RESET
		);
	}
	
	public static void warning(String message) {
		stream.println(
			ConsoleColor.YELLOW_BRIGHT + "[!]" + prefix
			+ ConsoleColor.YELLOW_BOLD + "[" + ConsoleColor.YELLOW_BRIGHT + formatter.format(new Date()) + ConsoleColor.YELLOW_BOLD + "]"
			+ ConsoleColor.YELLOW + " > " + message+ ConsoleColor.RESET
		);
	}
	
	public static void error(String message) {
		stream.println(
			ConsoleColor.RED_BRIGHT + "[X]" + prefix
			+ ConsoleColor.RED_BOLD + "[" + ConsoleColor.RED_BRIGHT + formatter.format(new Date()) + ConsoleColor.RED_BOLD + "]"
			+ " > " + ConsoleColor.RED + message+ ConsoleColor.RESET
		);
	}
	
	public static void info(String message) {
		stream.println(
			ConsoleColor.BLUE + "[?]" + prefix
			+ ConsoleColor.BLUE_BRIGHT + "[" + ConsoleColor.CYAN_BRIGHT + formatter.format(new Date()) + ConsoleColor.BLUE_BRIGHT + "]"
			+ " > " + ConsoleColor.CYAN + message+ ConsoleColor.RESET
		);
	}
	
	public static void question(boolean resetColor) {
		stream.print(
			ConsoleColor.PURPLE + "[?]" + prefix
			+ ConsoleColor.PURPLE_BRIGHT + "[" + ConsoleColor.PURPLE + formatter.format(new Date()) + ConsoleColor.PURPLE_BRIGHT + "]"
			+ " ? " + (resetColor ? ConsoleColor.RESET : "")
		);
	}
	
	public static void success(String message) {
		stream.println(
			ConsoleColor.GREEN_BRIGHT + "[~]" + prefix
			+ ConsoleColor.GREEN_BOLD + "[" + ConsoleColor.WHITE_BRIGHT + formatter.format(new Date()) + ConsoleColor.GREEN_BOLD + "]"
			+ " > " + ConsoleColor.GREEN_BRIGHT + message+ ConsoleColor.RESET
		);
	}
	
	private static String prefix = " ";
	
	public static void setPrefix(String prefix) { JamLogger.prefix = prefix; }
	
	public static void setOut(PrintStream out) {
		stream = out;
	}
	
	public static void resetOut(PrintStream out) {
		stream = System.out;
	}

}
