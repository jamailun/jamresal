import fr.jamailun.jamlogger.JamLogger;
import fr.jamailun.jamResal.Packet;
import fr.jamailun.jamResal.PacketStructure;
import fr.jamailun.jamResal.server.JamServer;

import java.io.IOException;

public class TestServer {
	
	public static void main(String[] args) {
		try {new TestServer();} catch (IOException e) {e.printStackTrace();}
	}
	
	private final JamServer server;
	
	TestServer() throws IOException {
		JamLogger.setPrefix("[SERVER]");
		server = new JamServer(8090, 16);
		server.callbacks()
				.addCallback(16, this::setUsername)
				.addCallback(15, this::receiveClientMessage)
				.setConnectionCallback(t -> {
					//JamLogger.success("Client connected !");
				})
				.setDisconnectionCallback(t -> {
					String username = t.getString("username");
					JamLogger.success("Client disconnected ("+username+") .");
					server.sendToAll(STRUCTURE_MESSAGE_SYSTEM.build(username + " s'est déconnecté."));
				});
		server.start();
	}
	
	private void setUsername(Packet p) {
		String username = p.readString();
		JamLogger.info("Username set : " + username);
		p.getTunnel().properties().setPropertie("username", username);
		p.getTunnel().sendPacket(new Packet(10));
		server.sendToAll(STRUCTURE_MESSAGE_SYSTEM.build(username + " has joined the tchat."));
	}
	
	private void receiveClientMessage(Packet p) {
		String message = p.readString();
		server.sendToAll(STRUCTURE_MESSAGE_RETRANSFER.build(p.getTunnel().properties().getString("username"), message));
	}
	
	private static final PacketStructure STRUCTURE_MESSAGE_RETRANSFER = new PacketStructure(14)
			.addField(PacketStructure.FieldType.STRING).addField(PacketStructure.FieldType.STRING);
	private static final PacketStructure STRUCTURE_MESSAGE_SYSTEM = new PacketStructure(13)
			.addField(PacketStructure.FieldType.STRING);
	
}
